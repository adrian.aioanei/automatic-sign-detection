#pragma once

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include<iostream>


class BasicImageOperations
{
public:
	static std::string _imagePath;
	static bool isImagePathValid(std::string path) { return path.empty(); }
	static std::string getCurrentImagePath() { return _imagePath; }
	static bool readImage(cv::Mat& currentImage, std::string imagePath = "");
	static bool getImageReprezentation(std::string imagePath, cv::Mat& currentImage);
	static cv::Mat convertToGrayScale(const cv::Mat& source);
	static void printErrorMessage(std::string filePath, int lineNumber, std::string message, std::string errorLevel="Default");
};
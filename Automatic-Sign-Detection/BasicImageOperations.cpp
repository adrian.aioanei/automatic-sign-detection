#include "BasicImageOperations.h"

std::string BasicImageOperations::_imagePath = "";

bool BasicImageOperations::getImageReprezentation(std::string imagePath, cv::Mat& output)
{
	if (isImagePathValid(imagePath))
		std::cerr << "No parameter for image path send. Try static path." << std::endl;
	else {
		output = cv::imread(imagePath);
		return true;
	}

	if (isImagePathValid(_imagePath))
		std::cerr << "Static parameter is not valid. [Exit]" << std::endl;
	else {
		output = cv::imread(_imagePath);
		return true;
	}

	output.data = nullptr;
	return false;
}

cv::Mat BasicImageOperations::convertToGrayScale(const cv::Mat & source)
{
	cv::Mat output;
	if (source.data == nullptr) {
		printErrorMessage(__FILE__, __LINE__, "Empty source image","ERROR");
		return cv::Mat();
	}
	cv::cvtColor(source, output, cv::COLOR_BGR2GRAY);
	return output;
}

void BasicImageOperations::printErrorMessage(std::string filePath, int lineNumber, std::string message, std::string errorLevel)
{
	std::cerr << "[ "<<errorLevel<<" ]" << std::endl <<
		"\t" << "In file : " << filePath << std::endl <<
		"\t" << "At line : " << lineNumber << std::endl <<
		"\t" << "Error message : " << message << std::endl;
}

bool BasicImageOperations::readImage(cv::Mat & output, std::string imagePath)
{
	return getImageReprezentation(imagePath, output);
}

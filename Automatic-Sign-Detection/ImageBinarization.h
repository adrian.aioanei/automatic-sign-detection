#pragma once

#include"BasicImageOperations.h"

class ImageBinarization : public BasicImageOperations
{
public:
	bool binImage(cv::Mat&currentImage, std::string path = "", bool showImage = false);
	ImageBinarization();
	~ImageBinarization();
};


#include "ImageBinarization.h"

ImageBinarization::ImageBinarization()
{
}

ImageBinarization::~ImageBinarization()
{
}

bool ImageBinarization::binImage(cv::Mat & output, std::string path, bool showImage)
{
	bool readStatus = readImage(output, path);
	output = convertToGrayScale(output);
	if (showImage && output.data != nullptr) {
		cv::imshow("Sample image", output);
		cv::waitKey(0);
	}
	return readStatus;
}